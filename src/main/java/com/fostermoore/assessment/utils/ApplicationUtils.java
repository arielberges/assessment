package com.fostermoore.assessment.utils;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class ApplicationUtils {

    public static final String DATA_URL = "https://jsonplaceholder.typicode.com/";

    public<T> T getUrlData(@NotNull String url, @NotNull Class<T> classType) {
        WebClient webClient = WebClient.create(url);
        Mono<T> response = webClient.get()
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(classType).log();
        return response.block();
    }
}
