package com.fostermoore.assessment.models;

import org.jetbrains.annotations.NotNull;

public class Company {

    private String name;
    private String catchPhrase;
    private String bs;

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @NotNull public String getCatchPhrase() {
        return catchPhrase;
    }

    public void setCatchPhrase(@NotNull String catchPhrase) {
        this.catchPhrase = catchPhrase;
    }

    @NotNull public String getBs() {
        return bs;
    }

    public void setBs(@NotNull String bs) {
        this.bs = bs;
    }
}
