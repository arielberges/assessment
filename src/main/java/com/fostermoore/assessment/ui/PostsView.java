package com.fostermoore.assessment.ui;

import com.fostermoore.assessment.models.Post;
import com.fostermoore.assessment.models.User;
import com.fostermoore.assessment.services.PostService;
import com.fostermoore.assessment.services.UserService;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * View in charged of displaying the Posts made by a particular user.
 *
 * @author Ariel Berges
 */
@Route("users/:userId/posts")
@AnonymousAllowed
public class PostsView extends VerticalLayout implements BeforeEnterObserver {

    private PostService postService;
    private UserService userService;

    private Long userId;

    public PostsView(
            @NotNull @Autowired PostService postService,
            @NotNull @Autowired UserService userService) {
        this.postService = postService;
        this.userService = userService;
    }

    @Override
    public void beforeEnter(@NotNull BeforeEnterEvent beforeEnterEvent) {
        try {
            userId = beforeEnterEvent.getRouteParameters().getLong("userId").get();
            List<Post> posts = postService.getPostsByUserId(userId);
            User user = userService.getUserById(userId);

            add(new H1("Posts made by " + user.getName()));
            Grid<Post> grid = new Grid<>(Post.class, false);
            grid.addColumn(Post::getTitle).setHeader("Title");
            grid.addColumn(Post::getBody).setHeader("Body");
            grid.setItems(posts);
            add(grid);
        } catch (Exception exception) {
            UI.getCurrent().getPage().setLocation("/error/");
        }
    }
}
