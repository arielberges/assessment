package com.fostermoore.assessment.ui;

import com.fostermoore.assessment.models.User;
import com.fostermoore.assessment.services.UserService;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * View in charged of handling actions related to the users.
 *
 * @author Ariel Berges
 */
@Route("users")
@AnonymousAllowed
public class UsersView extends VerticalLayout {

    private UserService userService;

    public UsersView(@Autowired @NotNull UserService userService) {
        this.userService = userService;

        add(new H1("Users View"));

        Grid<User> grid = new Grid<>(User.class, false);
        grid.addColumn(User::getName).setHeader("Name");
        grid.addColumn(User::getUsername).setHeader("Username");
        grid.addColumn(user -> user.getCompany().getName()).setHeader("Company");

        grid.addComponentColumn(user -> {
            Button button = new Button(new Icon(VaadinIcon.EYE));
            button.addClickListener(clickEvent -> {
                UI.getCurrent().getPage().setLocation("/users/" + user.getId() + "/posts");
            });

            return button;
        }).setHeader("View Posts");

        grid.setItems(userService.getUsers());

        add(grid);
    }
}
