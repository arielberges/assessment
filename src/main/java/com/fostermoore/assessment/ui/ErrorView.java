package com.fostermoore.assessment.ui;

import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;

/**
 * View in charged of displaying errors.
 * Everytime an error happens, we want the user to be notified with a proper message
 *
 * @author Ariel Berges
 */
@Route("error")
@AnonymousAllowed
public class ErrorView extends VerticalLayout {

    public ErrorView() {
        add(new H1("Something went wrong"));
        add(new H1("We should show something better here"));
    }
}
