package com.fostermoore.assessment.services;

import com.fostermoore.assessment.utils.ApplicationUtils;
import com.fostermoore.assessment.models.User;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Service to handle Users
 *
 * @author Ariel Berges
 */
@Service
public class UserServiceImpl implements UserService {

    private static final String USERS_URL = ApplicationUtils.DATA_URL + "users/";
    private static final String USER_ID_URL = USERS_URL + "%d/";

    @Autowired
    private ApplicationUtils utils;

    @Nullable
    public User getUserById(@NotNull Long id) {
        User user = utils.getUrlData(String.format(USER_ID_URL, id), User.class);
        return user;
    }

    @NotNull
    public List<User> getUsers() {
        User[] usersArray = utils.getUrlData(USERS_URL, User[].class);
        List<User> users = Arrays.stream(usersArray).toList();
        return users;
    }
}

