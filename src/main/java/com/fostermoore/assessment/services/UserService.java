package com.fostermoore.assessment.services;

import com.fostermoore.assessment.models.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;


/**
 * Service to handle Users
 *
 * @author Ariel Berges
 */
public interface UserService {

    /**
     * Return a particular user by id
     *
     * @param id - User identifier
     */
    @Nullable User getUserById(@NotNull Long id);

    /**
     * Return the whole list of users
     */
    @NotNull List<User> getUsers();

}
