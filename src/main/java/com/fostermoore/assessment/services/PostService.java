package com.fostermoore.assessment.services;

import com.fostermoore.assessment.models.Post;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Service to handle Posts
 *
 * @author Ariel Berges
 */
public interface PostService {

    /**
     * Return the whole list of posts made by a particular user
     *
     * @param userId - User identifier
     */
    @NotNull List<Post>  getPostsByUserId(@NotNull Long userId);

}
