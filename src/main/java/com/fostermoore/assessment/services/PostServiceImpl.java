package com.fostermoore.assessment.services;

import com.fostermoore.assessment.utils.ApplicationUtils;
import com.fostermoore.assessment.models.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.List;

/**
 * Service to handle Posts
 *
 * @author Ariel Berges
 */
@Service
public class PostServiceImpl implements PostService {

    private static final String POST_URL = ApplicationUtils.DATA_URL + "users/%d/posts/";

    @Autowired
    private ApplicationUtils utils;

    @NotNull
    public List<Post> getPostsByUserId(@NotNull Long userId) {
        Post[] postsArray = utils.getUrlData(String.format(POST_URL, userId), Post[].class);
        List<Post> posts = Arrays.stream(postsArray).toList();
        return posts;
    }
}

