package com.fostermoore.assessment.services;

import com.fostermoore.assessment.models.Post;
import com.fostermoore.assessment.utils.ApplicationUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.mockito.Mockito.when;

/**
 * Tests for {@link PostServiceImpl}.
 *
 * @author Ariel Berges
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class PostServiceTest extends AbstractJUnit4SpringContextTests {

    @Mock
    private ApplicationUtils utils;

    @InjectMocks
    private final PostService postService = new PostServiceImpl();

    @Mock
    private Post post1;
    @Mock
    private Post post2;

    @Before
    public void setup() {
        when(post1.getId()).thenReturn(1L);
        when(post2.getId()).thenReturn(2L);
    }

    @Test
    public void getPostsByUserId_userHasPosts_returnPostList() {
        when(utils.getUrlData("https://jsonplaceholder.typicode.com/users/1/posts/", Post[].class))
                .thenReturn(new Post[]{ post1, post2 });
        List<Post> posts = postService.getPostsByUserId(1L);

        Assert.assertEquals(2, posts.size());
        Assert.assertEquals(post1, posts.get(0));
        Assert.assertEquals(post2, posts.get(1));
    }

    @Test
    public void getPostsByUserId_userDoesNotHavePosts_returnEmptyList() {
        when(utils.getUrlData("https://jsonplaceholder.typicode.com/users/5/posts/", Post[].class))
                .thenReturn(new Post[]{});
        List<Post> posts = postService.getPostsByUserId(5L);
        Assert.assertTrue(posts.isEmpty());
    }
}
