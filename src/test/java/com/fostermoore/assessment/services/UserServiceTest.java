package com.fostermoore.assessment.services;

import com.fostermoore.assessment.models.User;
import com.fostermoore.assessment.utils.ApplicationUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.List;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link UserServiceImpl}.
 *
 * @author Ariel Berges
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest extends AbstractJUnit4SpringContextTests {

    @Mock
    private ApplicationUtils utils;

    @InjectMocks
    private final UserService userService = new UserServiceImpl();

    @Mock
    private User user1;
    @Mock
    private User user2;

    @Before
    public void setup() {
        when(user1.getId()).thenReturn(1L);
        when(user2.getId()).thenReturn(2L);
    }

    @Test
    public void getUsers_whenUsersExist_returnUserList() {
        when(utils.getUrlData("https://jsonplaceholder.typicode.com/users/", User[].class))
                .thenReturn(new User[]{ user1, user2 });
        List<User> users = userService.getUsers();

        Assert.assertEquals(2, users.size());
        Assert.assertEquals(user1, users.get(0));
        Assert.assertEquals(user2, users.get(1));
    }

    @Test
    public void getUserById_whenUserExists_returnUserOk() {
        when(utils.getUrlData("https://jsonplaceholder.typicode.com/users/1/", User.class))
                .thenReturn(user1);
        User user = userService.getUserById(1L);
        Assert.assertEquals(Long.valueOf(1), user.getId());
    }


    @Test
    public void getUserById_whenUserDoesNotExist_returnNull() {
        when(utils.getUrlData("https://jsonplaceholder.typicode.com/users/5/", User.class))
                .thenReturn(null);
        User user = userService.getUserById(5L);
        Assert.assertNull(user);
    }
}
